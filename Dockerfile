FROM ubuntu:xenial

RUN apt-get update \
    && apt-get install -y software-properties-common python-software-properties wget apt-transport-https\
    && wget -nc https://dl.winehq.org/wine-builds/Release.key \
    && apt-key add Release.key \
    && apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ xenial main'

RUN dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get install -y --allow-unauthenticated wine-staging=5.5~xenial winehq-staging=5.5~xenial \
            wine-staging-i386=5.5~xenial  wine-staging-amd64=5.5~xenial winbind cabextract xdotool python3-pip
RUN mkdir -p /root/steamcmd && cd /root/steamcmd \
	&& wget http://media.steampowered.com/installer/steamcmd_linux.tar.gz \
	&& tar -zxvf steamcmd_linux.tar.gz \
	&& rm -f steamcmd_linux.tar.gz
RUN wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks \
  && chmod +x winetricks \
  && mv -v winetricks /usr/bin

RUN echo 233780 > steam_appid.txt

COPY requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt

VOLUME /arma3
VOLUME /root/.wine

ENV VALIDATE=1
ENV DISPLAY=novnc:0

WORKDIR /

COPY . /deploy
COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
