import configparser
import json
import os
from pathlib import Path


class SettingsLoader:
    SCRIPTPATH = os.path.dirname(os.path.realpath(__file__))

    def load_ini_settings_file(self):
        settings = configparser.ConfigParser()
        settings_file = Path(self.SCRIPTPATH) / 'settings' / 'settings.ini'
        if not settings_file.exists():
            raise FileNotFoundError("Please create a settings.ini file")

        settings.read(str(settings_file))
        return settings

    def load_modlist_config(self):
        file = Path(self.SCRIPTPATH) / 'settings' / 'modlist.json'
        if not file.exists():
            raise FileNotFoundError("Please create a 'modlist.json' file")

        with file.open() as json_file:
            return json.load(json_file)

    def load_ini_settings(self):
        """Gets a dictionary with all data we need for server"""
        settings = self.load_ini_settings_file()
        data = {
            'arma_workshop_id': settings.get('Static', 'arma_workshop_id'),
            'arma_server_id': settings.get('Static', 'arma_server_id'),
            'steamcmd_path': settings.get('Static', 'steamcmd_path'),
            'use_wine': settings.get('Static', 'use_wine', fallback='false').lower() == 'true',
            'server_exe': settings.get('Static', 'server_exe'),
            'steam_password': settings.get('Account', 'steam_password'),
            'steam_accountname': settings.get('Account', 'steam_accountname'),
        }
        return data
