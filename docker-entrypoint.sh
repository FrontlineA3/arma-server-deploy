#!/bin/bash

set -e

setup() {
  # move installed steamcmd to volume so it is not updated with each deploy build
  if [ ! -d /arma3/steamcmd ]; then
    copy_steamcmd
  fi

  # build wine if not available yet
  if [ ! -d /root/.wine/drive_c ]; then
    wineboot -i &
    wineboot_clicks
    install_winetricks &
    winetricks_installation_clicks
  fi

  # install steam client if it is missing
  if [ ! -d /root/.wine/drive_c/Program\ Files/Steam ]; then
    install_steam
  fi

  # make symbolic link to arma3 dir in the wine drive
  if [ ! -d /root/.wine/drive_c/arma3 ]; then
    ln -s /arma3 /root/.wine/drive_c/arma3
  fi
}

build_server() {
  python3 /deploy/deploy.py "$@" --actions=build
  chmod +x /arma3/$1/launchserver.sh
}

update_server() {
  python3 /deploy/deploy.py "$@" --actions=update
}

update_server_mods() {
  python3 /deploy/deploy.py "$@" --actions=update_server_mods
}

update_mods() {
  python3 /deploy/deploy.py "$@" --actions=update_mods
}

update_missions() {
  python3 /deploy/deploy.py "$@" --actions=update_missions
}

copy_steamcmd() {
  echo "Copying steamcmd to volume"
  cp -r /root/steamcmd /arma3/steamcmd
}

install_winetricks() {
  winetricks vcrun2013 vcrun2015 d3dx11_43 mdac28 xact
}

wineboot_clicks() {
  # mono + gecko
  xdotool sleep 5 key KP_Enter sleep 25 key KP_Enter sleep 15
}

winetricks_installation_clicks() {
  # vcrun2013
  sleep 10
  xdotool key alt+a alt+i sleep 3 key alt+c
  # vcrun2015
  sleep 15
  xdotool key alt+a alt+i sleep 3 key alt+c
  # d3dx11_43
  sleep 20
  # mdac28
  xdotool key space alt+n sleep 3
  eval $(xdotool getwindowgeometry --shell `xdotool search --name "Microsoft data"`)
  xdotool mousemove $(($WIDTH + $X - 140)) $(($HEIGHT + $Y - 20)); xdotool click 1
  sleep 10
  xdotool mousemove $(($WIDTH + $X - 140)) $(($HEIGHT + $Y - 20)); xdotool click 1
  sleep 10
}

install_steam() {
  wget https://steamcdn-a.akamaihd.net/client/installer/SteamSetup.exe
  wine SteamSetup.exe &
  steam_installation_clicks
  rm SteamSetup.exe
}

steam_installation_clicks() {
  xdotool sleep $STEAM_INSTALL_LOAD_DELAY key --delay 50 KP_Enter KP_Enter KP_Enter \
          sleep $STEAM_INSTALL_PROCESS_DELAY key --delay 50 space KP_Enter
}

update_steam_client() {
  # simply run steam client for 30 seconds and then kill container
  wine "C:\Program Files\Steam\Steam.exe" &
  sleep 30
}

tail_logs() {
  export RPTFILE=`ls -t /arma3/"$1"/profiles/ | head -1`
  echo "############### Displaying logs for ${RPTFILE} ##################"
  tail -F /arma3/"$1"/profiles/"${RPTFILE}"
}

setup
"$@"
