"""
    Does a bunch of stuff, related to TS. Installs, creates channel and whatever
"""

import os
import sys
import platform
import urllib.request
import zipfile
import tarfile
from deploy import get_settings
from imports.colorsLog import ColorPrint

SCRIPTPATH = os.path.dirname(os.path.realpath(__file__))
settings = get_settings()
colorprint = ColorPrint()

class Teamspeak(object):
    """
    Handles teamspeak setup
    """

    def __init__(self, install_path):
        """
        :param install_path: installation path for teamspeak
        """
        colorprint.yellow("Downloading teamspeak server")
        self.install_path = install_path
        self.platform = platform.system()
        if self.platform == 'Windows':
            self.filename = 'teamspeak3-server_win64-3.4.0.zip'
            self.teamspeak_url = 'http://dl.4players.de/ts/releases/3.4.0/' + self.filename

        elif self.platform == 'Linux':
            self.filename = "teamspeak3-server_linux_amd64-3.4.0.tar.bz2"
            self.teamspeak_url = "http://dl.4players.de/ts/releases/3.4.0/" + self.filename

        self.downloadpath = os.path.join(SCRIPTPATH, "downloadcache", self.filename)
        if not os.path.isdir(os.path.join(SCRIPTPATH, "downloadcache")):
            os.mkdir(os.path.join(SCRIPTPATH, "downloadcache"))

    def _download_teamspeak(self):
        try:
            return urllib.request.urlretrieve(self.teamspeak_url, self.downloadpath)
        except Exception as e:
            raise Exception(
                'An unknown exception occurred! {}'.format(e))

    def _extract_teamspeak(self):
        colorprint.yellow("Extracting teamspeak")
        if self.platform == 'Windows':
            with zipfile.ZipFile(self.downloadpath, 'r') as f:
                return f.extractall(self.install_path)

        elif self.platform == 'Linux':
            with tarfile.open(self.downloadpath, 'r:gz') as f:
                return f.extractall(self.install_path)

    def _add_to_firewall(self):
        """Adds firewall exception to the install path"""
        colorprint.yellow("Adding firewall exception")
        if sys.platform == "win32":
            os.popen('netsh advfirewall firewall add rule name="{name}" dir=in action=allow program="{path}" enable=yes profile=any'.format(
                name='Teamspeak 3 Server',
                path=os.path.join(self.install_path, 'teamspeak3-server_win64', "ts3server.exe"),
            ))

    def install(self, force=True):
        self._download_teamspeak()
        self._extract_teamspeak()
        self._add_to_firewall()

def teamspeak_install():
    """
        Installs this stuff
    """
    # If the path is empty, set it to SteamCMD in the current folder
    colorprint.magenta("Installing Teamspeak")
    tspath = settings.get('Teamspeak', 'teamspeak_path')

    if tspath == "''" or tspath == "":
        tspath = os.path.join(SCRIPTPATH, 'installed')
        if not os.path.isdir:
            os.mkdir(tspath)
        settings.set('Teamspeak', 'teamspeak_path', tspath)
        with open(os.path.join(SCRIPTPATH, "settings", "settings.ini"), "w") as configfile:
            settings.write(configfile)

    colorprint.yellow(tspath)
    teamspeak = Teamspeak(tspath)
    teamspeak.install(True)
