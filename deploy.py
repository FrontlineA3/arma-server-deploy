"""
    Creates a new setup for an arma server, creates the config files and so on
"""
import argparse
import os
import subprocess

from imports.colorsLog import ColorPrint
from server_manager import ServerManager
from settings_loader import SettingsLoader

colorprint = ColorPrint()

def server_toggle(servicename, enable=False):
    """
    TODO: probably this one is deprecated and should be removed - consider doing so later
    """
    try:
        if enable:
            colorprint.green("\nStarting {}".format(servicename))
            subprocess.call(
                ('net start "DaemonMaster_{}"'.format(servicename)))
        else:
            colorprint.green("\nStopping {}".format(servicename))
            subprocess.call(
                ('net stop "DaemonMaster_{}"'.format(servicename)))
    except:
        pass


def main(config, settings, server_names, actions, args):
    """
    :param config: config loaded from modlist.json
    :param settings: settings loaded from .ini file
    :param server_names: servers for which actions are being performed
    :param actions: list of actions to be performed on the servers
    :param args: additional args
    """
    """Update a given server"""
    for servername in server_names:
        server_toggle(config[servername]['servicename'], False)

    for servername in server_names:
        server_actions = ServerManager(config, settings, servername, args)
        server_actions.run(actions)

    for servername in server_names:
        server_toggle(config[servername]['servicename'], True)

    colorprint.green("Done!")


if __name__ == "__main__":
    script_path = os.path.dirname(os.path.realpath(__file__))
    settings_loader = SettingsLoader()
    config = settings_loader.load_modlist_config()
    settings = settings_loader.load_ini_settings()

    parser = argparse.ArgumentParser(description="Updates workshop mods for server")
    parser.add_argument('server', help="Which server to update",
                        choices=config.keys(), nargs="+")
    parser.add_argument('--actions', help="What actions should be performed",
                        choices=ServerManager.ACTIONS.keys(), nargs="+", default="build", required=True)
    parser.add_argument('--validate', help="Validate arma install", action="store_true", required=False, default=False)
    parser.add_argument('--skip-download', help="Skips workshop downloads", action="store_true", required=False,
                        default=False)
    args = parser.parse_args()

    main(config, settings, args.server, args.actions, args)
