import errno
import glob
import json
import os
import platform
import shutil
import subprocess
import sys
import zipfile
from shutil import copy2

import requests
from tqdm import tqdm

from adapters import PathAdapter, ServerSettings
from imports.colorsLog import ColorPrint

colorprint = ColorPrint()


def wrap(arg):
    """Wraps in double quotes"""
    return "'" + arg + "'"


class Server(object):
    def __init__(self, settings: ServerSettings):
        self.settings = settings
        self.servername = self.settings.name
        self.serverpath = self.settings.path
        self.configpath = self.serverpath
        self.port = self.settings.port
        self.skipdownload = False
        self.mods = []

    def get_path(self, *args):
        return PathAdapter(self.settings.use_wine, *args)

    def create_configs(self):
        """This copies over the template configs"""
        colorprint.yellow("Creating configs")
        script_path = os.path.dirname(os.path.realpath(__file__))
        templatepath = self.get_path(script_path, "templates").native

        bat_template = self.get_path(templatepath, "launchserver.bat")
        sh_template = self.get_path(templatepath, "launchserver.sh")
        par_template = self.get_path(templatepath, "modlist.txt")
        config_template = self.get_path(templatepath, "config_server.cfg")
        cfg_template = self.get_path(templatepath, "config_basic.cfg")
        servercfg_template = self.get_path(templatepath, 'serverconfig.json.tpl')
        daemon_template = self.get_path(templatepath, "config_daemon.dmdf")

        bat_target = self.get_path(self.configpath, "launchserver.bat")
        sh_target = self.get_path(self.configpath, "launchserver.sh")
        par_target = self.get_path(self.configpath, "config_modlist.txt")
        config_target = self.get_path(self.configpath, "config_server.cfg")
        cfg_target = self.get_path(self.configpath, "config_basic.cfg")
        servercfg_target = self.get_path(os.path.join(self.configpath, 'serverconfig', 'Frontline'), "serverconfig.json")
        daemon_target = self.get_path(self.configpath, "config_daemon.dmdf")
        profile_target = self.get_path(self.configpath, "profiles")

        # Write the .bat file we use to launch the server
        with open(bat_template.native, "r") as launchbat:
            bat_source = launchbat.read()
            bat_data = bat_source.format(
                parpath=par_target.adapted,
                configpath=config_target.adapted,
                cfgpath=cfg_target.adapted,
                profilepath=profile_target.adapted,
                port=self.settings.port,
                armapath=self.get_path(self.serverpath, self.settings.server_exe_file).adapted
            )
            with open(bat_target.native, "w") as launchbatnew:
                launchbatnew.write(bat_data)

        # Write the .bat file we use to launch the server
        with open(sh_template.native, "r") as tpl_f:
            source = tpl_f.read()
            data = source.format(
                parpath=par_target.adapted,
                configpath=config_target.adapted,
                cfgpath=cfg_target.adapted,
                profilepath=profile_target.adapted,
                port=self.settings.port,
                armapath=self.get_path(self.serverpath, self.settings.server_exe_file).adapted
            )
            with open(sh_target.native, "w") as f:
                f.write(data)

        # Create frontline server mod config file
        FRONTLINE_CONFIG_PARAMS = [
            'FRONTLINE_CONFIG_RADIO_CHANNEL_NAME',
            'FRONTLINE_CONFIG_RADIO_ENFORCED',
            'FRONTLINE_CONFIG_RADIO_CHANNEL_PASSWORD',
            'FRONTLINE_CONFIG_RADIO_ADDRESS',
            'FRONTLINE_CONFIG_ADMIN_PASSWORD',
            'FRONTLINE_CONFIG_AFKICK_TIME',
            'FRONTLINE_CONFIG_AFKICK_ENABLED',
        ]

        if not os.path.exists(os.path.dirname(servercfg_target.native)):
            try:
                os.makedirs(os.path.dirname(servercfg_target.native))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        with open(servercfg_template.native, "r") as tpl_f:
            source = tpl_f.read()
            tpl_params = {
                key: os.getenv(
                    "{}_{}".format(self.servername.upper(), key),  # get the value for server config TYPE_KEY
                    os.getenv(
                        "DEFAULT_{}".format(key), ''  # or fallback and get DEFAULT_KEY value otherwise
                    )
                ) for key in FRONTLINE_CONFIG_PARAMS
            }
            data = source % tpl_params
            with open(servercfg_target.native, "w") as f:
                f.write(data)

        # Write the .bat file we use to launch the server
        with open(daemon_template.native, 'r') as daemonfile:
            daemon_data = json.load(daemonfile)
            daemon_data['DisplayName'] = self.settings.service_name
            daemon_data['ServiceName'] = "DaemonMaster_{}".format(self.settings.service_name)
            daemon_data['Description'] = self.settings.config.host_name,
            daemon_data['FileDir'] = self.serverpath
            daemon_data['FullPath'] = self.get_path(self.serverpath, self.settings.server_exe_file).adapted
            daemon_data['Parameter'] = "-port={port} -autoInit - enableHT " \
                                       "\"-config={config_target}\" \"-cfg={cfg_target}\" " \
                                       "\"-par={par_target}\" \"-profiles={profile_target}\", ".format(
                port=self.settings.port,
                config_target=config_target.adapted,
                cfg_target=cfg_target.adapted,
                par_target=par_target.adapted,
                profile_target=profile_target.adapted
            )

            with open(daemon_target.native, "w") as daemonfilenew:
                json.dump(daemon_data, daemonfilenew, sort_keys=True, indent=4, ensure_ascii=False)

        # Write the .par file containing most of the arguments, makes it easier to edit things you'd actually want to adjust
        with open(par_template.native, "r") as launchpar:
            par_source = launchpar.read()
            par_data = par_source.format(modlist='')
            with open(par_target.native, "w") as launchparnew:
                launchparnew.write(par_data)

        # Copy the basic config file, that just has some stuff we don't want to change
        copy2(config_template.native, config_target.native)
        copy2(cfg_template.native, cfg_target.native)

    def update_mod(self, mod, path, validate=False):
        """
        :param mod: Name for the mod
        :param modid: Workshop id for the mod
        :param path: Path to install the mod to
        """

        colorprint.magenta("\nUpdating mod {} [{}]".format(mod.name, mod.steam_id))
        modpath = os.path.join(path, "@{}".format(mod.name))

        if platform.system() == 'Linux':
            steamcmd = "steamcmd.sh"
        else:
            steamcmd = "steamcmd.exe"

        steamcmd_params = [os.path.join(self.settings.steam_cmd_path, steamcmd)]

        if platform.system() == 'Linux' and self.settings.use_wine:
            steamcmd_params.append("+@sSteamCmdForcePlatformType windows")

        steamcmd_params.extend([
            "+force_install_dir \"{}\"".format(os.path.join(self.settings.steam_cmd_path, 'steam')),
            "+login {} {}".format(self.settings.steam_username, self.settings.steam_password),
            "+workshop_download_item {} {}".format(self.settings.arma_workshop_id, mod.steam_id),
            "validate" if validate else "",
            "+quit"
        ])

        try:
            _exitcode = subprocess.check_call(steamcmd_params)

            # Create a symlink if it doenst exist yet
            modpath_target = os.path.join(path, "@{}".format(mod.name))
            modpath_source = os.path.join(
                self.settings.steam_cmd_path, "steam", "steamapps", "workshop", "content",
                self.settings.arma_workshop_id, mod.steam_id
            )
            if not os.path.islink(modpath_target):
                os.symlink(modpath_source, modpath_target,
                           target_is_directory=True)
            self.copy_keys(modpath, path)
        except subprocess.CalledProcessError as e:
            if "TIMEOUT" in e.returncode:
                colorprint.error("Timeout downloading mod: {}".format(mod.name))
                colorprint.error("Restarting the same download: {}".format(mod.name))
                self.update_mod(mod, path, validate=validate)
            else:
                colorprint.error("Error downloading mod: {}".format(mod.name))
                colorprint.error("Continuing with next mod")

    def copy_keys(self, modpath, serverdir):
        """Copies bikeys from a mod, to the keys folder in the arma folder"""
        dst_dir = os.path.join(serverdir, "keys")
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)

        for bikey in glob.iglob(os.path.join(modpath, "**", "*.bikey")):
            file_ = os.path.basename(bikey)
            colorprint.yellow("Copying keys: {}".format(file_))
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                os.remove(dst_file)
            copy2(bikey, dst_dir)


    def download_remote_file(self, url, file_path):
        # Streaming, so we can iterate over the response.
        req = requests.get(url, stream=True)
        # Total size in bytes.
        total_size = int(req.headers.get('content-length', 0))
        block_size = 1024  # 1KB
        bar = tqdm(total=total_size, unit='iB', unit_scale=True)
        with open(file_path, 'wb') as f:
            for data in req.iter_content(block_size):
                bar.update(len(data))
                f.write(data)
        bar.close()
        if total_size != 0 and bar.n != total_size:
            colorprint.error("ERROR, something went wrong")

    def unzip_file(self, zipped_file, target_path):
        with zipfile.ZipFile(zipped_file, 'r') as f:
            f.extractall(target_path)

    def download_server_mod(self, mod):
        # download mod from server manually and unzip
        colorprint.blue("Downloading mod {} from server".format(mod.name))
        filename = "{}.{}".format(mod.name, mod.url.split(".")[-1])
        file_path = os.path.join(self.serverpath, filename)

        self.download_remote_file(mod.url, file_path)

        dst = os.path.join(self.serverpath, '@{}'.format(mod.name))
        if os.path.exists(dst):
            shutil.rmtree(dst)

        self.unzip_file(file_path, self.serverpath)

        if mod.zip_inner_dir:
            src = os.path.join(self.serverpath, mod.zip_inner_dir)
            dst = os.path.join(self.serverpath, "@{}".format(mod.name))
            if os.path.exists(dst):
                shutil.rmtree(dst)
            os.rename(src, dst)

        mod_path = os.path.join(self.serverpath, "@{}".format(mod.name))
        self.copy_keys(mod_path, self.serverpath)

    def install_pythia_requirements(self, mod_names):
        """
        Installs extra requirements in the mods dependent on Pythia mod.
        :param mod_dir_name: @name list of mods installed by this script
        """
        if not self.settings.use_wine:
            colorprint.blue("Install extra requirements for each dependant mod manually with bat file in @Pythia dir")
            return

        pythia_path = os.path.join(self.serverpath, "@Pythia")
        if not os.path.exists(pythia_path):
            colorprint.grey("Pythia mod is not installed. Maybe you should add it to your modlist config "
                            "if you are using mods that require it?")

        for mod in mod_names:
            requirements_path = os.path.join(self.serverpath, mod, "requirements.txt")
            if os.path.exists(requirements_path):
                colorprint.blue("Installing extra requirements for Pythia from mod {mod}".format(mod=mod))
                os.system(
                    'wine cmd /c '
                    '"C:\\arma3\\{server}\\@Pythia\\python-37-embed-win32\\python.exe" -m '
                    'pip install -r "C:\\arma3\\{server}\\{mod}\\requirements.txt"'.format(
                        server=self.servername,
                        mod=mod
                    )
                )

    def process_mods_setup(self, mods, validate):
        mod_names = []

        for mod in mods:
            if not self.skipdownload:
                if mod.custom:
                    self.download_server_mod(mod)
                else:
                    self.update_mod(mod, self.serverpath, validate=validate)

            mod_names.append("@{}".format(mod.name))

        return mod_names

    def mods_setup(self, validate):
        return self.process_mods_setup(self.settings.mods, validate)

    def server_mods_setup(self, validate):
        mod_names = self.process_mods_setup(self.settings.server_mods, validate)
        self.install_pythia_requirements(mod_names)
        return mod_names

    def all_mods_setup(self, validate=False):
        """Downloads mods, links them to the folder, copy the keys and add them to startup mod list"""
        colorprint.yellow("Setting up mod launch params")
        modnames = self.mods_setup(validate)
        modnamesserver = self.server_mods_setup(validate)

        par_target = os.path.join(self.configpath, "config_modlist.txt")
        with open(par_target, "w") as par_file:
            if modnames:
                par_file.write("-mod={}\n".format(";".join(modnames)))
            if modnamesserver:
                par_file.write("-servermod={}\n".format(";".join(modnamesserver)))
            if self.settings.file_patching:
                par_file.write("-filePatching\n")

    def update_missions(self):
        colorprint.blue("Updating missions")
        for index, mission in enumerate(self.settings.missions):
            filename = "mission_pack{}.{}".format(index, mission.url.split(".")[-1])
            file_path = os.path.join(self.serverpath, filename)
            self.download_remote_file(mission.url, file_path)
            missions_path = os.path.join(self.serverpath, "mpmissions")
            if os.path.exists(missions_path):
                shutil.rmtree(missions_path)

            self.unzip_file(file_path, missions_path)

    def config_update(self):
        """Updates the primary config for the server"""
        data = self.settings.config.data
        config_target = os.path.join(self.configpath, "config_server.cfg")
        with open(config_target, "r") as config_file:
            config_data = config_file.read()

        for key, value in data.items():
            try:
                index = config_data.index(key, 0)
                insertpoint = config_data.index(";", index)
                if isinstance(value, list):
                    insert = "{}[] = {open}{value}{close};".format(
                        key, value=",\n".join(wrap(v) for v in value), open="{", close="}"
                    )
                elif isinstance(value, str):
                    insert = "{} = \"{}\";".format(key, value)
                elif isinstance(value, int):
                    insert = "{} = {};".format(key, value)

                config_data = config_data[:index] + insert + config_data[insertpoint+1:]

            except:
                colorprint.error("Unknown setting used: {}".format(key))

        with open(config_target, "w") as config_file:
            config_file.write(config_data)

    def add_firewall_allow(self):
        """Adds firewall rule for this arma installation"""
        if sys.platform == "win32":
            os.popen(
                'netsh advfirewall firewall add rule name="{name}" dir=in action=allow program="{path}" enable=yes profile=any'.format(
                    name="Arma - {}".format(self.servername),
                    path=os.path.join(self.serverpath, self.settings.server_exe_file),
                ))

    def update_arma(self, validate=False):
        """Installs/updates arma"""
        if self.skipdownload:
            return 0

        if validate:
            validatetext = " validate"
        else:
            validatetext = ""

        current_dir = os.getcwd()
        os.chdir(self.settings.steam_cmd_path)

        colorprint.magenta("Updating Arma 3")
        colorprint.yellow('Installing to: {}'.format(self.serverpath))

        if platform.system() == 'Linux':
            steamcmd = "steamcmd.sh"
        else:
            steamcmd = "steamcmd.exe"

        steamcmd_params = [os.path.join(self.settings.steam_cmd_path, steamcmd)]

        if platform.system() == 'Linux' and self.settings.use_wine:
            steamcmd_params.append("+@sSteamCmdForcePlatformType windows")

        steamcmd_params.extend([
            "+login {} {}".format(self.settings.steam_username, self.settings.steam_password),
            "+force_install_dir \"{}\"".format(self.serverpath),
            "+app_update {}{}".format(self.settings.arma_server_id, validatetext),
            "+quit"
        ])
        try:
            dummy_exitcode = subprocess.check_call(steamcmd_params)
            self.add_firewall_allow()
        except subprocess.CalledProcessError as error:
            colorprint.error(error.returncode)
            colorprint.error("Error installing arma, continuing for now")
        return 0
