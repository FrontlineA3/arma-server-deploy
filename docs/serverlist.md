## Base stuff
* [X] Download steamCMD

## New Arma Server instance
* [X] Start CMD and do the following
```
force_install_dir "C:\Arma 3 - Vanilla"
app_update 233780 validate
```
* [X] Add firewall exception for arma3server_x64.exe
* [X] Create symlinks for all mods from `steamCMD\steamapps\workshop\content\107410` folder (The cool mod updater should create symlinks)
* [ ] Set up missions <<STACK>>
* [ ] Set up launcher + server mods <<STACK>>
    * [ ] Clib
    * [ ] python\missionFinder
    * [ ] DynamicFrontline
    * [ ] Frontline_server`
        * [ ] Serverconfig.json
* [ ] Battleeye(??)
* [ ] Server config
    * [x] TADST_basic.cfg (no adjust)
    * [x] TADST_config.cfg (Create a template and fill in from server setup) [Password, mission name, server name]
* [X] Startup parameters
```
-port=2202 "-config=C:\Arma 3 - Frontline_RHS\TADST\default\TADST_config.cfg" "-cfg=C:\Arma 3 - Frontline_RHS\TADST\default\TADST_basic.cfg" "-profiles=C:\Arma 3 - Frontline_RHS\TADST\default" -name=default -filePatching "-mod=@Frontline_server;@Bozcaada;@CUP Terrains - Core;@CUP_Terrains_Maps;@frl_kokan;@frl_kunduz;@frl_bornholmwest;@frl_saaremaa;@frl_terrainobjects;@frl_wandarovka;@Frontline_compat_RHS;@Pulau;@RHSAFRF;@RHSGREF;@RHSUSAF;@Ruha" "-servermod=@Clib_Server;@Pythia;@DynamicFrontline" -autoInit -enableHT
```
https://community.bistudio.com/wiki/Startup_Parameters_Config_File

* [ ] Teamspeak channel creation


## The great migration
* [ ] Missions CI <stack>
    * [ ] Cygwin?
    * [ ] Set variables on https://gitlab.com/FrontlineA3/missions/settings/ci_cd
    * [ ] Create symlinks from target location to MPMissions(?)
* [ ] Backup TS, set it up on new server
* [ ] DNS (Versio adanteh account)


## Manual shit done
* Install 7Zip
* Install goodSync (Super legal dundy copy)
    * Set up Goodsync Connect, to easily access files everywhere
    * user: an.postmus@gmail.com, pass: FRL2017goodsync
* Install Atom
* Install Python3.7
* Copy arma_server_deploy
* Run python .\setup.py
* Copy TS database from old dedi to new one (Using the goodsync thing)
* Run python .\deploy.py vanilla rhs
* Install directX from `https://www.microsoft.com/en-ca/download/details.aspx?id=35`
* Install VC redist from `https://www.microsoft.com/en-us/download/details.aspx?id=40784`
  
* Cygwin
    * install cygwin from `https://cygwin.com/setup-x86_64.exe`
    * start cygwin
    * run `ssh-host-config`
    * cyg_server / FRL2017cygwin
    * net start sshd
* Created windows account: armaadmin / lingermarketperformA1!
* Add an SSH key to connect
    * Create key on dundy PC with PuttyGen
    * RDP to armaadmin
    * Copy the public key from the field and paste in armaadmin/.ssh/authorized_keys (Export no work)
    * Save private key to PC
    * In Putty under SSH/Auth/Key select the ppk file

* Copy server mods [@DynamicFrontline]
* Copy clean @Pythia from local
* Drag requirements.txt from @DynamicFrontline to install_requirements.bat in pythia
* Copy serverconfig/Frontline file
*

## New server instance
* RDP to the worldstream dedi
    * Add to modlist.json as 'something'
    * Run python .\deploy.py something 
    * Copy [DynamicFrontline, Pythia, python, serverconfig] folders from RHS to new server path
    * In DaemonMaster do Add and Import daemon_config.dmdf from the server path
    * In serverconfig\Frontline\serverconfig.json set a new Teamspeak channel