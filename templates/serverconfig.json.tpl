{
	"radio": {
		"channelname": "%(FRONTLINE_CONFIG_RADIO_CHANNEL_NAME)s",
		"password": "%(FRONTLINE_CONFIG_RADIO_CHANNEL_PASSWORD)s",
		"enforced": %(FRONTLINE_CONFIG_RADIO_ENFORCED)s,
		"ip": "%(FRONTLINE_CONFIG_RADIO_ADDRESS)s"
	},

	"admin": {
		"password": "%(FRONTLINE_CONFIG_ADMIN_PASSWORD)s"
	},

	"afkick": {
		"enabled": %(FRONTLINE_CONFIG_AFKICK_ENABLED)s,
		"time": %(FRONTLINE_CONFIG_AFKICK_TIME)s
	}
}
