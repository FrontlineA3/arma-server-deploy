# Installation

## Configuration and CLI
1. Copy `settings_sample` directory to `settings` and provide proper `modlist.json` and `settings.ini` files configuration inside
1. Copy `.env.example` to `.env` and fill out all blanks
1. Copy `bin/asd` file to your `$HOME/bin/` and provide `u+x` permissions on the file -- this will provide you with handy CLI
1. Provide your own `docker-compose.override.yaml` file based on `docker-compose.yaml` file. 

## Building

1. Build docker images and setup shared volumes (like Steam installation): `asd setup`
1. Build target server with `asd build TARGET` where `TARGET` is the name of the server service in docker-compose

## Running the server

1. Start servers: `asd start TARGET`

# Updating the game servers

Later you may want to update server or missions 
```bash
$ asd update  # will update all running instances -- should be used instead of build after the first build
$ asd update_servers NAMES  # will update only selected services
$ asd update_missions  # will pull missions for all running instances - does not require servers restarts!
```

# CLI 

CLI wraps the use of docker-compose and other commands to simplify management. See more commands:
```bash
$ asd help  # for more commands
```


# Developing deploy script

In order to develop deploy script, you will need to use different docker-compose file. Copy `docker-compose.development.yaml` to `docker-compose.override.yaml` and provide adjustments (directories for volumes). Then follow Installation process above. 

You can use VNC for debugging process of the wine apps. Run VNC: `docker-compose up -d vnc` (You can check proper URL to VNC with `docker-compose logs vnc`)

