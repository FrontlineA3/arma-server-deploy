import ntpath
import os
import platform


class PathAdapter:
    def __init__(self, use_wine, *args):
        self.args = args
        self.use_wine = use_wine

    @property
    def native(self):
        return os.path.join(*self.args)

    @property
    def adapted(self):
        if platform.system() == 'Linux' and self.use_wine:
            args = (arg.replace('/', '\\') for arg in self.args)
            return 'C:' + ntpath.join(*args)

        return os.path.join(*self.args)


class Mod:
    def __init__(self, mod_name, mod_data):
        self.name = mod_name
        self.data = mod_data

    @property
    def url(self):
        if self.custom:
            return self.data.get("url")
        return None

    @property
    def custom(self):
        return isinstance(self.data, dict)

    @property
    def steam_id(self):
        return self.data  # TODO: change to more structured data later

    @property
    def zip_inner_dir(self):
        if self.url and "zipInnerDir" in self.data:
            return self.data["zipInnerDir"]
        return None


class Mission:
    def __init__(self, mission_data):
        self.mission_data = mission_data

    @property
    def url(self):
        return self.mission_data["url"]


class ServerConfig:
    def __init__(self, data):
        self.data = data

    @property
    def host_name(self):
        return self.data["hostName"]

    @property
    def password_admin(self):
        return self.data["passwordAdmin"]

    @property
    def server_command_password(self):
        return self.data["serverCommandPassword"]

    @property
    def password(self):
        return self.data["password"]

    @property
    def max_players(self):
        return self.data["maxPlayers"]

    @property
    def allowed_file_patching(self):
        return self.data["allowedFilePatching"]

    @property
    def disable_VoN(self):
        return self.data["disableVoN"]

    @property
    def battle_eye(self):
        return self.data["BattleEye"]

    @property
    def verify_signatures(self):
        return self.data["verifySignatures"]

    @property
    def template(self):
        return self.data["template"]

    @property
    def motd(self):
        return self.data["motd"]


class ServerSettings:
    def __init__(self, config_data, ini_settings, server_name):
        try:
            self.data = config_data[server_name]
        except KeyError:
            print("Cannot access config data for given server name = {}".format(server_name))
            raise

        self.name = server_name
        self.ini = ini_settings

    @property
    def steam_cmd_path(self):
        return self.ini.get("steamcmd_path")

    @property
    def steam_username(self):
        return self.ini.get("steam_accountname")

    @property
    def steam_password(self):
        return self.ini.get("steam_password")

    @property
    def arma_workshop_id(self):
        return self.ini.get("arma_workshop_id")

    @property
    def arma_server_id(self):
        return self.ini.get("arma_server_id")

    @property
    def use_wine(self):
        return self.ini.get("use_wine")

    @property
    def server_exe_file(self):
        return self.ini.get("server_exe")

    @property
    def service_name(self):
        return self.data["servicename"]

    @property
    def path(self):
        return self.data.get("path")

    @property
    def port(self):
        return self.data.get("port", 2302)

    @property
    def cygwin_path(self):
        return self.data["cygwin_path"]

    @property
    def file_patching(self):
        return self.data.get("filePatching", False)

    @property
    def config(self):
        return ServerConfig(self.data["config"])

    @property
    def mods(self):
        return [
            Mod(mod_name, mod_data)
            for mod_name, mod_data in self.data["mods"].items()
        ]

    @property
    def server_mods(self):
        return [
            Mod(mod_name, mod_data)
            for mod_name, mod_data in self.data["servermods"].items()
        ]

    @property
    def missions(self):
        return [
            Mission(mission) for mission in self.data["missions"]
        ]


class IniSettings:
    def __init__(self, settings):
        pass
