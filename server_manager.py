from adapters import ServerSettings
from server import Server


class ServerManager:
    def __init__(self, config, ini_settings, servername, args):
        self.config = ServerSettings(config, ini_settings, servername)

        self.server = Server(self.config)

        self.server.skipdownload = args.skip_download
        self.args = args

    def run(self, actions):
        for action in actions:
            try:
                self.ACTIONS.get(action)(self)
            except KeyError:
                print("Cannot run action {} - it was not found".format(action))

    def build(self):
        self.update_arma()
        self.server.create_configs()
        self.server.all_mods_setup(
            self.args.validate,
        )
        self.server.update_missions()
        self.server.config_update()

    def update(self):
        self.update_arma()
        self.update_mods()
        self.update_server_mods()
        self.update_missions()

    def update_arma(self):
        self.server.update_arma(self.args.validate)

    def update_mods(self):
        self.server.mods_setup(
            self.args.validate,
        )

    def update_server_mods(self):
        self.server.server_mods_setup(
            self.args.validate,
        )

    def update_missions(self):
        self.server.update_missions()

    ACTIONS = {
        "build": build,
        "update": update,
        "update_arma": update_arma,
        "update_mods": update_mods,
        "update_server_mods": update_server_mods,
        "update_missions": update_missions,
    }
