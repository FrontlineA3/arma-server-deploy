"""
    Does all the setup required for this to work, 
    should only be run once for each actual dedi
"""

import os
import configparser
import platform
import urllib.request
import zipfile
import tarfile
import subprocess
from pathlib import Path

from imports.colorsLog import ColorPrint
from settings_loader import SettingsLoader
from teamspeak import teamspeak_install

SCRIPTPATH = os.path.dirname(os.path.realpath(__file__))
settings_loader = SettingsLoader()
SETTINGS = settings_loader.load_ini_settings()
colorprint = ColorPrint()

class Steamcmd(object):
    def __init__(self, install_path):
        """
        :param install_path: installation path for steamcmd
        """
        self.install_path = install_path
        self.platform = platform.system()
        if self.platform == 'Windows':
            self.steamcmd_url = 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip'
            self.filename = 'steamcmd.zip'
            self.steamcmd_exe = os.path.join(self.install_path, 'steamcmd.exe')

        elif self.platform == 'Linux':
            self.steamcmd_url = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz"
            self.filename = 'steamcmd.tar.gz'
            self.steamcmd_exe = os.path.join(self.install_path, 'steamcmd.sh')


        self.downloadpath = os.path.join(SCRIPTPATH, "downloadcache", self.filename)
        if not os.path.isdir(os.path.join(SCRIPTPATH, "downloadcache")):
            os.mkdir(os.path.join(SCRIPTPATH, "downloadcache"))

    def _download_steamcmd(self):
        try:
            return urllib.request.urlretrieve(self.steamcmd_url, self.downloadpath)
        except Exception as e:
            raise Exception(
                'An unknown exception occurred! {}'.format(e))

    def _extract_steamcmd(self):
        if self.platform == 'Windows':
            with zipfile.ZipFile(self.downloadpath, 'r') as f:
                return f.extractall(self.install_path)

        elif self.platform == 'Linux':
            with tarfile.open(self.downloadpath, 'r:gz') as f:
                return f.extractall(self.install_path)

    def install(self, force=True):
        """Downloads and installs steamcmd"""
        self._download_steamcmd()
        self._extract_steamcmd()

    def update(self):
        """Opens up steamCMD to update it"""
        subprocess.check_call([self.steamcmd_exe, "+quit"])

def steamcmd_install():
    """
        Installs this stuff
    """
    # If the path is empty, set it to SteamCMD in the current folder
    colorprint.magenta("Installing steamCMD")
    steamcmdpath = SETTINGS['steamcmd_path']
    if steamcmdpath == "''" or steamcmdpath == "":
        steamcmdpath = os.path.join(SCRIPTPATH, 'installed', "SteamCMD")
        if not os.path.isdir(steamcmdpath):
            os.mkdir(steamcmdpath)

        settings = configparser.ConfigParser()
        settings.set('Static', 'steamcmd_path', steamcmdpath)
        settings_file = Path(SCRIPTPATH) / 'settings' / 'settings.ini'
        if not settings_file.exists():
            raise FileNotFoundError("Please create a settings.ini file")
        settings.read(str(settings_file))
        
        with settings_file.open(mode='w') as configfile:
            settings.write(configfile)

    colorprint.yellow(steamcmdpath)
    steamcmd = Steamcmd(steamcmdpath)
    steamcmd.install(True)
    steamcmd.update()

if __name__ == "__main__":
    steamcmd_install()
    if SETTINGS.getboolean('Teamspeak', 'install_ts', fallback=False):
        teamspeak_install()
